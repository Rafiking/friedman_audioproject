﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving : MonoBehaviour
{
    private bool direct = true;
    public float speed = 5.0f;
    public AudioClip dribble;
    public AudioClip shoot1;
    public AudioClip shoot2;

    //public AudioSource stream1, stream2;

    // Start is called before the first frame update
    void Start()
    {
        //stream1.clip = dribble;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("Player").transform.position.x >= 25) 
            speed = 0;
        else
            speed = 5.0f;

        if (direct)
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        else
            transform.Translate(-Vector3.forward * speed * Time.deltaTime);

        if (transform.position.z >= 42.0f)
        {
            direct = false;
            //stream1.Pause();
            //stream2.clip = shoot1;
            //stream2.Play();
            //AudioSource.PlayClipAtPoint(shoot1, gameObject.transform.position);
        }

        if (transform.position.z <= -42.0f)
        {
            direct = true;

            //AudioSource.PlayClipAtPoint(shoot2, gameObject.transform.position);
        }
    }
}
