﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioPlayerTrigger : MonoBehaviour
{
    public AudioMixer mixer;
    public AudioMixerSnapshot[] snapshots;
    public float[] weights;
    public float transitionlength = 1f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("MusicTrigger"))
            //print("Ya!");
              weights[0] = 0.0f;
              weights[1] = 1.0f;
              mixer.TransitionToSnapshots(snapshots, weights, transitionlength);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("MusicTrigger"))
            weights[0] = 1.0f;
            weights[1] = 0.0f;
            mixer.TransitionToSnapshots(snapshots, weights, transitionlength);
    }
}
